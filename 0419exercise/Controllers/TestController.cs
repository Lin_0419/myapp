﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _0419exercise.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "123";
            ViewBag.Name = "Lin";

            ViewData["A"] = 1;
            ViewData["B"] = 2;

            ViewBag.A = 1;
            ViewBag.B = 2;
            return View();
        }
        public ActionResult HTml()
        {
            return View();
        }
        public ActionResult Htmlhelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }

    
    }
}